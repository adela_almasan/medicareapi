package com.medicare.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QZipCode is a Querydsl query type for ZipCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QZipCode extends EntityPathBase<ZipCode> {

    private static final long serialVersionUID = -940082026L;

    public static final QZipCode zipCode = new QZipCode("zipCode");

    public final StringPath city = createString("city");

    public final StringPath county = createString("county");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath state = createString("state");

    public final StringPath zip3 = createString("zip3");

    public final StringPath zip5 = createString("zip5");

    public final StringPath zipLookupCode = createString("zipLookupCode");

    public QZipCode(String variable) {
        super(ZipCode.class, forVariable(variable));
    }

    public QZipCode(Path<? extends ZipCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QZipCode(PathMetadata metadata) {
        super(ZipCode.class, metadata);
    }

}

