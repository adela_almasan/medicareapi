package com.medicare.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRate is a Querydsl query type for Rate
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRate extends EntityPathBase<Rate> {

    private static final long serialVersionUID = -137815560L;

    public static final QRate rate = new QRate("rate");

    public final StringPath age = createString("age");

    public final NumberPath<Double> ageForSorting = createNumber("ageForSorting", Double.class);

    public final NumberPath<Double> annualRate = createNumber("annualRate", Double.class);

    public final NumberPath<Integer> area = createNumber("area", Integer.class);

    public final StringPath company = createString("company");

    public final StringPath companyOld = createString("companyOld");

    public final StringPath coupleFac = createString("coupleFac");

    public final DateTimePath<java.util.Date> effDate = createDateTime("effDate", java.util.Date.class);

    public final StringPath gender = createString("gender");

    public final StringPath highestRate = createString("highestRate");

    public final StringPath householdDiscount = createString("householdDiscount");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lowestRate = createString("lowestRate");

    public final NumberPath<Double> monthlyRate = createNumber("monthlyRate", Double.class);

    public final NumberPath<Double> naic = createNumber("naic", Double.class);

    public final StringPath plan = createString("plan");

    public final StringPath policyFee = createString("policyFee");

    public final NumberPath<Double> quarterlyRate = createNumber("quarterlyRate", Double.class);

    public final StringPath rateType = createString("rateType");

    public final NumberPath<Double> semiAnnualRate = createNumber("semiAnnualRate", Double.class);

    public final StringPath state = createString("state");

    public final StringPath tobacco = createString("tobacco");

    public final ListPath<ZipCode, QZipCode> zipCodes = this.<ZipCode, QZipCode>createList("zipCodes", ZipCode.class, QZipCode.class, PathInits.DIRECT2);

    public final StringPath zipLookupCode = createString("zipLookupCode");

    public QRate(String variable) {
        super(Rate.class, forVariable(variable));
    }

    public QRate(Path<? extends Rate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRate(PathMetadata metadata) {
        super(Rate.class, metadata);
    }

}

