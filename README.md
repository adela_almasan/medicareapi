# README #

### What is this repository for? ###

* Medicare Supplement Rates API
* Version 1.0

### Set up ###

* Dependencies: Java 8, Maven, MySQL
* Clone git repository locally
* Create a MySQL database to be added in configuration file
* Configure the datasource url(`spring.datasource.url`), username (`spring.datasource.username`) and password(`spring.datasource.password`) in `application.properties` file 
* Execute `mvn clean install` - this will compile, run the tests and build the artifact

### Running the application ###
* Execute the `main()` method from `Application` class

### Who do I talk to? ###

* Adela Almasan (ela.almasan@gmail.com)