package com.medicare.resource;

import com.medicare.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Test class for the RateController REST controller.
 *
 * @see RateController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class RateControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    String gender = "M";
    String age = "70";
    String zipCode = "37024";
    String tobacco = "Y";

    ResultMatcher ok = MockMvcResultMatchers.status().isOk();
    ResultMatcher bad = MockMvcResultMatchers.status().is4xxClientError();

    @Test
    public void testGetRatesByZipCodeAge() throws Exception{
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/medigap?age=70&zip_code=37024");
        this.mockMvc.perform(builder)
                .andExpect(ok);
    }

    @Test
    public void testGetRatesByAllParams() throws Exception{
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/medigap?age=70&zip_code=37024&tobacco=Y&gender=M");
        this.mockMvc.perform(builder)
                .andExpect(ok);
    }

    @Test
    public void testGetRatesMissingParams() throws Exception{
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/medigap?tobacco=Y&gender=M");
        this.mockMvc.perform(builder)
                .andExpect(bad);
    }

}