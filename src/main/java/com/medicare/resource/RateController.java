package com.medicare.resource;

import com.medicare.model.Rate;
import com.medicare.service.RateService;
import com.medicare.vm.SearchRateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RateController {

    public RateController() {
        System.out.println("test");
    }

    @Autowired
    private RateService rateService;

    @GetMapping("/medigap")
    public Page<Rate> getRates(SearchRateRequest searchRateRequest, Pageable pageable, @RequestParam("zip_code") String zipCode) {
        searchRateRequest.setZipCode(zipCode);
        return rateService.findRates(searchRateRequest, pageable);
    }

}
