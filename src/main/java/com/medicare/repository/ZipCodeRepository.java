package com.medicare.repository;

import com.medicare.model.ZipCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipCodeRepository extends JpaRepository<ZipCode, Long> {
    @Override
    ZipCode findOne(Long aLong);

    @Override
    ZipCode getOne(Long aLong);

}
