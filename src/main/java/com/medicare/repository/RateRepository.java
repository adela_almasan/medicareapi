package com.medicare.repository;

import com.medicare.model.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RateRepository extends JpaRepository<Rate, Long>, QueryDslPredicateExecutor<Rate> {
        List<Rate> findAll();
}
