package com.medicare.vm;

import javax.validation.constraints.NotNull;

public class SearchRateRequest {

    @NotNull
    private String zipCode;
    @NotNull
    private String age;
    private String gender;
    private String tobacco;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTobacco() {
        return tobacco;
    }

    public void setTobacco(String tobacco) {
        this.tobacco = tobacco;
    }
}
