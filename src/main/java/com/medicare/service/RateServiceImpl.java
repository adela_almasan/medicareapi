package com.medicare.service;

import com.medicare.model.QRate;
import com.medicare.model.QZipCode;
import com.medicare.model.Rate;
import com.medicare.repository.RateRepository;
import com.medicare.vm.SearchRateRequest;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class RateServiceImpl implements RateService {

    @Autowired
    private RateRepository rateRepository;

    private final Logger log = LoggerFactory.getLogger(RateServiceImpl.class);

    @Override
    public List<Rate> getAllRates() {
        return rateRepository.findAll();
    }

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Rate> findRates(SearchRateRequest request, Pageable pageable) {
        QZipCode zipCode = QZipCode.zipCode;
        QRate rate = QRate.rate;

        JPQLQuery<Rate> query = new JPAQuery<>(entityManager);

        BooleanExpression mandatoryExpression = rate.zipLookupCode.eq(zipCode.zipLookupCode)
                .and(zipCode.zip5.eq(request.getZipCode()))
                .and(rate.age.eq(request.getAge()));

        BooleanBuilder booleanBuilder = new BooleanBuilder(mandatoryExpression);

        if (request.getGender() != null) {
            if (request.getGender().equals("M"))
                request.setGender("Male");
            else
                request.setGender("Female");

            booleanBuilder.and(rate.gender.eq(request.getGender()));
        }

        if (request.getTobacco() != null) {
            if (request.getTobacco().equals("Y"))
                request.setTobacco("Tobacco");
            else
                request.setTobacco("Non-Tobacco");

            booleanBuilder.and(rate.tobacco.eq(request.getTobacco()));
        }

        QueryResults<Rate> results = query.from(rate)
                .leftJoin(rate.zipCodes, zipCode)
                .where(booleanBuilder)
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();

        return new PageImpl<>(results.getResults(), pageable, results.getTotal());

    }

}
