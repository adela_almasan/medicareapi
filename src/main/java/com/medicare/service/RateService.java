package com.medicare.service;

import com.medicare.model.Rate;
import com.medicare.vm.SearchRateRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RateService {

    List<Rate> getAllRates();

    Page<Rate> findRates(SearchRateRequest searchRateRequest, Pageable pageable);

}
