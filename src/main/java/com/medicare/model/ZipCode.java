package com.medicare.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "zip_codes")
public class ZipCode implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "zip_codes_id")
    private long id;

    @Column(name = "zip_lookup_code")
    private String zipLookupCode;

    @Column(name = "state")
    private String state;

    @Column(name = "county")
    private String county;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_3")
    private String zip3;

    @Column(name = "zip_5")
    private String zip5;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZipLookupCode() {
        return zipLookupCode;
    }

    public void setZipLookupCode(String zipLookupCode) {
        this.zipLookupCode = zipLookupCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip3() {
        return zip3;
    }

    public void setZip3(String zip3) {
        this.zip3 = zip3;
    }

    public String getZip5() {
        return zip5;
    }

    public void setZip5(String zip5) {
        this.zip5 = zip5;
    }

}
