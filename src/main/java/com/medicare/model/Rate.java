package com.medicare.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "rates")
public class Rate implements Serializable {

    @OneToMany(fetch = EAGER)
    @JoinColumn(name = "zip_lookup_code", referencedColumnName = "zip_lookup_code")
    private List<ZipCode> zipCodes;
    @Id
    @GeneratedValue
    @Column(name = "rate_id")
    private long id;
    @Column(name = "company")
    private String company;
    @Column(name = "company_old")
    private String companyOld;
    @Column(name = "naic")
    private Double naic;
    @Column(name = "plan")
    private String plan;
    @Column(name = "state")
    private String state;
    @Column(name = "area")
    private Integer area;
    @Column(name = "zip_lookup_code")
    private String zipLookupCode;
    @Column(name = "gender")
    private String gender;
    @Column(name = "tobacco")
    private String tobacco;
    @Column(name = "couple_fac")
    private String coupleFac;
    @Column(name = "eff_date")
    private Date effDate;
    @Column(name = "rate_type")
    private String rateType;
    @Column(name = "age_for_sorting")
    private Double ageForSorting;
    @Column(name = "lowest_rate")
    private String lowestRate;
    @Column(name = "highest_rate")
    private String highestRate;
    @Column(name = "age")
    private String age;
    @Column(name = "monthly_rate")
    private Double monthlyRate;
    @Column(name = "quarterly_rate")
    private Double quarterlyRate;
    @Column(name = "semi_annual_rate")
    private Double semiAnnualRate;
    @Column(name = "annual_rate")
    private Double annualRate;
    @Column(name = "policy_fee")
    private String policyFee;
    @Column(name = "household_discount")
    private String householdDiscount;

    public List<ZipCode> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(List<ZipCode> zipCodes) {
        this.zipCodes = zipCodes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyOld() {
        return companyOld;
    }

    public void setCompanyOld(String companyOld) {
        this.companyOld = companyOld;
    }

    public Double getNaic() {
        return naic;
    }

    public void setNaic(Double naic) {
        this.naic = naic;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getZipLookupCode() {
        return zipLookupCode;
    }

    public void setZipLookupCode(String zipLookupCode) {
        this.zipLookupCode = zipLookupCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTobacco() {
        return tobacco;
    }

    public void setTobacco(String tobacco) {
        this.tobacco = tobacco;
    }

    public String getCoupleFac() {
        return coupleFac;
    }

    public void setCoupleFac(String coupleFac) {
        this.coupleFac = coupleFac;
    }

    public Date getEffDte() {
        return effDate;
    }

    public void setEffDte(Date effDte) {
        this.effDate = effDte;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public Double getAgeForSorting() {
        return ageForSorting;
    }

    public void setAgeForSorting(Double ageForSorting) {
        this.ageForSorting = ageForSorting;
    }

    public String getLowestRate() {
        return lowestRate;
    }

    public void setLowestRate(String lowestRate) {
        this.lowestRate = lowestRate;
    }

    public String getHighestRate() {
        return highestRate;
    }

    public void setHighestRate(String highestRate) {
        this.highestRate = highestRate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Double getMonthlyRate() {
        return monthlyRate;
    }

    public void setMonthlyRate(Double monthlyRate) {
        this.monthlyRate = monthlyRate;
    }

    public Double getQuarterlyRate() {
        return quarterlyRate;
    }

    public void setQuarterlyRate(Double quarterlyRate) {
        this.quarterlyRate = quarterlyRate;
    }

    public Double getSemiAnnualRate() {
        return semiAnnualRate;
    }

    public void setSemiAnnualRate(Double semiAnnualRate) {
        this.semiAnnualRate = semiAnnualRate;
    }

    public Double getAnnualRate() {
        return annualRate;
    }

    public void setAnnualRate(Double annualRate) {
        this.annualRate = annualRate;
    }

    public String getPolicyFee() {
        return policyFee;
    }

    public void setPolicyFee(String policyFee) {
        this.policyFee = policyFee;
    }

    public String getHouseholdDiscount() {
        return householdDiscount;
    }

    public void setHouseholdDiscount(String householdDiscount) {
        this.householdDiscount = householdDiscount;
    }


}